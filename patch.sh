#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT="$(basename "$0")"

sdat_dir=$1
log_file=""

# Use these environment variables to use specific executables for the respective tools
if [[ "$exe_7za" == "" ]] ; then exe_7za=7za ; fi
if [[ "$exe_make_npdata" == "" ]] ; then exe_make_npdata=make_npdata ; fi

# Setting this environment variable to 0 will skip deletion 
#   of the patched zip files and directories
if [[ "$del_temp" == "" ]] ; then del_temp=1 ; fi

function die
{
	echo "$@" >&2
	[[ "$log_file" != "" ]] && echo "Die $@" >> "$log_file"
	exit 1
}

function info
{
	echo "$@"
	[[ "$log_file" != "" ]] && echo "$@" >> "$log_file"
}

function dir_to_zip
{
	local zip_out="$(realpath "$1")"
	local dir_in="$2"
	
	info "Will zip '$dir_in' -> '$zip_out'"
	
	local owd="$(pwd)"
	cd "$dir_in" || die "Dir '$dir_in' does not exist"
	# this syntax is more or less undocumented ... 
	#  The important point here is to set the word size to 16 with deflate compression.
	#  This seems to be needed to get the game to load, otherwise it will crash during bootup.
	"$exe_7za" a -bb3 -tzip -mm=Deflate:fb16 -mx3 "$zip_out" . >> "$log_file" || die "Zip failed"
	
	# get some really verbose stats on the zip file just in case
	"$exe_7za" l -slt "$zip_out" >> "$log_file"
	cd "$owd"
}

function zip_to_dir
{
	local dir_out="$1"
	local zip_in="$(realpath "$2")"
	
	if [[ ! -d "$dir_out" ]] ; then
		die "Output dir '$dir_out' for '$zip_in' did not exist!"
	fi
	
	info "Will unpack '$zip_in' -> '$dir_out'"
	
	local owd="$(pwd)"
	cd "$dir_out"
	"$exe_7za" x -bb3 "$zip_in" >> "$log_file" || die "Unpack of '$zip_in' to '$dir_out' failed"
	cd "$owd"
}

function sdat_to_zip
{
	local sdat_file="$1"
	local zip_file="$2"
	
	local sdat_klic=0
	
	info "Will un-scramble '$sdat_file' -> '$zip_file'"
	
	# make_npdata [-v] -d <input> <output> <klic> <rap>
	"$exe_make_npdata" -v -d "$sdat_file" "$zip_file" $sdat_klic >> "$log_file"
	
	if [[ ! -f "$zip_file" ]] ; then
		die "Creating zip '$zip_file' from '$sdat_file' failed"
	fi
}

function zip_to_sdat
{
	local zip_file="$1"
	local sdat_file="$2"

	info "Will scramble '$zip_file' -> '$sdat_file'"

	# https://www.psdevwiki.com/ps3/EDAT_files
	# $ xxd Frontend.SDAT | head | sed -e "s/^/# /"
	# 00000000: 4e50 4400 0000 0002 0000 0000 0000 0000  NPD.............
	# 00000010: 0000 0000 0000 0000 0000 0000 0000 0000  ................
	# 00000020: 0000 0000 0000 0000 0000 0000 0000 0000  ................
	# 00000030: 0000 0000 0000 0000 0000 0000 0000 0000  ................

	local sdat_version=2
	local sdat_license_type=0
	local sdat_type=00
	local sdat_format=1
	local sdat_block=16
	local sdat_compress=0
	# yes that thing is empty
	local sdat_cid=""
	local sdat_klic=0

	# make_npdata [-v] -e <input> <output> <version> <license>
	#                           <type> <format> <block> <compress>
	#                           <cID> <klic> <rap>

	"$exe_make_npdata" -v -e "$zip_file" "$sdat_file" $sdat_version $sdat_license_type \
		"$sdat_type" $sdat_format $sdat_block $sdat_compress \
		"$sdat_cid" "$sdat_klic" >> "$log_file"

	if [[ ! -f "$sdat_file" ]] ; then
		die "Creating SDAT '$sdat_file' from '$zip_file' failed"
	fi
}

function patch_sdat
{
	local sdat=$1
	local md5=$2
		
	set -- "${@:2}"
	set -- "${@:2}"
	
	local sdat_abs="$sdat_dir/$sdat"
	local sdat_abs_patched="${sdat_abs}-patched"
	
	if [[ ! -f "$sdat_abs" ]] ; then
		die "Could not find '$sdat_abs', bailing out"
	fi
	
	md5sum "$sdat_abs" >> "$log_file"
	echo "$md5  $sdat_abs" | md5sum -c &>/dev/null
	if [[ "$?" != "0" ]] ; then
		die "MD5 sum of '$sdat' did not match will not continue"
	fi
	
	info "MD5 sum of '$sdat' matches, we are go!"
	
	local tmp_dir="$sdat_dir/tmp-${sdat}"
	local tmp_zip="$sdat_dir/tmp-${sdat}.zip"
	
	rm -rfv "$tmp_dir" "$tmp_zip" "$sdat_abs_patched" >> "$log_file"
	mkdir -p "$tmp_dir" >> "$log_file"
	
	sdat_to_zip "$sdat_abs" "$tmp_zip"
	zip_to_dir "$tmp_dir" "$tmp_zip"
	
	local owd="$(pwd)"
	cd "$tmp_dir"
	
	find . -type f -exec md5sum "{}" \; >> "$log_file"
	
	for p in "$@" ; do
		local p_file="$SCRIPT_DIR/$p"
		test -f "$p_file" || die "Patch file '$p_file' not found"
		info "Applying $p"
		patch -p0 --binary < "$p_file" >> "$log_file" || die "Patch $p failed"
	done
	
	find . -type f -exec md5sum "{}" \; >> "$log_file"
	
	cd "$owd"
	
	rm -f "$tmp_zip" >> "$log_file"
	dir_to_zip "$tmp_zip" "$tmp_dir"
	zip_to_sdat "$tmp_zip" "$sdat_abs_patched"
	
	if [[ "$del_temp" == 1 ]] ; then
		rm -fr "$tmp_zip" "$tmp_dir" | tee -a "$log_file"
	fi
	
	info "Patched '$sdat' -> '$sdat_abs_patched' successfully"
}

# This contains all the patches we are going to do on all files ...
declare -A sdat_patch_eu
sdat_patch_eu[Frontend.SDAT]="661b4ed73468ed1ea4cdefcb8557899c BLES00512-Frontend.patch"
sdat_patch_eu[C01_NinjaVillage_Main.SDAT]="bbdac1933c0b452700fdd72a47362515 BLES00512-Level01.patch"


if [[ ! -d "$sdat_dir" ]] ; then
	info "This script patches SDAT files from the PS3 release of mini ninjas to add a horizontal invert option."
	info "To do this copy the following SDAT files from the game disc to a directory:"
	for sdat in "${!sdat_patch_eu[@]}"; do 
		info "  '$sdat'"
	done
	info ""
	info "Then give the directory as an argument to this script, like so:"
	info "./$SCRIPT the_dir_with_all_the_sdats/"
	info ""
	info "Each SDAT file will be extracted, patched and then repacked into the same dir."
	info "The original files will not be overwritten, but instead new SDAT files created."
	info "Replace the original SDAT files on the PS3 game disc with the patched ones"
	info "(make sure to use the exact same names as the originals) and you are ready to go!"
	info ""
	info "For this script to work you will need a few tools:"
	info "  make_npdata (get it here https://github.com/aniruddh22/make_npdata this script was tested with v1.2)"
	info "  7za (your distribution probably has a package, or go here for windows https://www.7-zip.org/a/7z1604-extra.7z)"
	info "  md5sum, find, patch (your distribution probably has a package for each)"
	info ""
	info "Note: This script was developed under Linux, but should work on Mac as well."
	info "      Windows users can have a look at Cygwin or MinGW or WSL 2 to get this running."
	info "Note: Currently only the European BLES00512 disc release is supported."
	info "Note: Only the main menu and the 1st level will be patched."
	
	exit 0
fi

sdat_dir="$(cd "$sdat_dir" ; pwd)"

if [[ ! -d "$sdat_dir" ]] ; then
	die "SDAT dir '$sdat_dir' does not exist"
fi

log_file="$sdat_dir/patch.log"
date > "$log_file"

info "Will look for SDAT to patch in '$sdat_dir'"
info "Will look for patch files in '$SCRIPT_DIR'"
info "Will log everything I do to '$log_file'"

for sdat in "${!sdat_patch_eu[@]}" ; do 
	patch_sdat "$sdat" ${sdat_patch_eu[$sdat]}
done

info "All good :) now replace the SDAT on the game disc with the patched ones and have fun!"
exit 0
