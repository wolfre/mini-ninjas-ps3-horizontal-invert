# Horizontal invert patch for Mini Ninjas (TM) on PS3

So I wanted to play Mini Ninjas (BLES00512) on PS3.
However when starting the game I noticed that the horizontal camera was inverted compared to what I've been used to.
Checking the options menu I found the vertical camera axis to be invertable, but not the horizontal one.
This is usually a deal breaker and I would have dropped the game.

However I thought it strange to just have one axis invertable and not the other.
So maybe the game (engine) could do it, and just the menu entries were missing.
A bit of googleing later I found out how to extract (and repack) the game resources.
Looking at the way the menus work I was able to add a horizontal invert option.
Yay finally I could play the game with the camera the way I'm used to :smile:

## Doing it yourself

This is not to endorse piracy, but to enhance the experience of playing your legally purchased game!

The [patch.sh](patch.sh) script will do most of the work four you.
You will need a few files from the game (disc) and a way to play your modified game on your PS3 of course.
In addition to this you also need:
- a way to run this scrip, i.e. a [bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) installation
- [make_npdata](https://github.com/aniruddh22/make_npdata), which is needed to handle SDAT files (this script was tested with v1.2 of it)
- 7za for handling zip files (your linux distribution probably has a package, windows users can download it [here](https://www.7-zip.org/a/7z1604-extra.7z))
- md5sum, find, patch (common utils probably already installed if you are on Mac or Linux)

If all of the above is setup, you will need to copy some SDAT files to be patched from the game (disc) to a local dir.
Running the script without arguments will give you a list of the SDAT files you need:

```bash
./patch.sh
```

Now with all the SDAT files copied to one dir, you can patch them like so:

```bash
./patch.sh /dir/with-all-sdat-files/
```

The script will compare the SDAT against expected [MD5s](https://en.wikipedia.org/wiki/MD5) before patching.
This makes sure that only the correct things are patched and that the results will be as expected.
Patching should be quick and will produce new files for each patched SDAT (not modifying the original files).
Additionally a log file will be created in the SDAT dir, useful for problem analysis.
Copy the patched SDAT back to the game (how to do that depends on how you chose to play backups on your PS3).
And you should have a new menu option under "controls" now.

## Notes

This script was developed under Linux, but should work on Mac as well. 
Windows users can have a look at Cygwin or MinGW or WSL 2 to get this running.

Currently only the European BLES00512 disc release is supported (if anyone wants to contribute, file an issue).

Only the main menu and the 1st level will be patched.

The [patch.sh](patch.sh) will look for tools in the *PATH* variable.
However you can override the location for make_npdata and 7za using environment variables.
E.g. with 7za and make_npdata installed in /opt/custom you could do this:

```bash
exe_7za=/opt/custom/7za exe_make_npdata=/opt/custom/make_npdata ./patch.sh /dir/with-all-sdat-files/
```
